//
//  Statusbar.swift
//  Mijnenveger
//
//  Created by David van Enckevort on 09-01-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import SpriteKit

class Statusbar: SKShapeNode {
    var minesLabel: String = "000" {
        didSet {
            left.text = minesLabel
        }
    }
    var timeElapsedLabel: String = "0:00" {
        didSet {
            right.text = timeElapsedLabel
        }
    }
    var statusLabel: String = "😀" {
        didSet {
            center.text = statusLabel
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    var size: CGSize {
        didSet {
            left.position = CGPoint(x: 15, y:  size.height / 2)
            center.position = CGPoint(x: size.width / 2, y:  size.height / 2)
            right.position = CGPoint(x: size.width - 15, y:  size.height / 2)
            path = CGPath(rect: CGRect(origin: .zero, size: size), transform: nil)
        }
    }

    init(size: CGSize) {
        self.size = size
        self.left = Statusbar.makeStatusbarLabel(size: size, alignment: .left)
        self.center = Statusbar.makeStatusbarLabel(size: size, alignment: .center)
        self.right = Statusbar.makeStatusbarLabel(size: size, alignment: .right)
        super.init()
        path = CGPath(rect: CGRect(origin: .zero, size: size), transform: nil)
        fillColor = .black
        lineWidth = 0
        name = "Statusbar"
        addChild(left)
        addChild(right)
        addChild(center)
    }


    private let left: SKLabelNode
    private let center: SKLabelNode
    private let right: SKLabelNode

    private static func makeStatusbarLabel(size: CGSize, alignment: SKLabelHorizontalAlignmentMode) -> SKLabelNode {
        let label = SKLabelNode()
        label.fontColor = .red
        label.fontSize = size.height / 2
        label.fontName = "AmericanTypewriter"
        label.verticalAlignmentMode = .center
        label.horizontalAlignmentMode = alignment
        switch alignment {
        case .center:
            label.name = "center"
            label.position = CGPoint(x: size.width / 2, y:  size.height / 2)
        case .left:
            label.name = "left"
            label.position = CGPoint(x: 15, y:  size.height / 2)
        case .right:
            label.name = "right"
            label.position = CGPoint(x: size.width - 15, y:  size.height / 2)
        }
        return label
    }
}
