//
//  Notification.swift
//  Mijnenveger
//
//  Created by David van Enckevort on 09-01-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import Foundation

extension Notification {
    static let newGame = NSNotification.Name("newGame")
    static let time = NSNotification.Name("time")
    static let elapsedTime = NSNotification.Name("elapsedTime")
}
