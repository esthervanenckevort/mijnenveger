//
//  MineSweeperBoard.swift
//  Mijnenveger
//
//  Created by David van Enckevort on 18-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit
import SpriteKit

class MineSweeperBoard: SKScene {
    weak var game: Game?
    private var map: [SKShapeNode] = []
    private let tileSize = CGSize(width: 80, height: 80)
    private let fontSize: CGFloat = 40.0
    private let colors: [UIColor] = [.blue, .green, .orange, .red, .purple, .brown, .darkGray, .black]
    private var zoomFactor: CGFloat = 1.0
    private var statusbar: Statusbar?
    private var background: SKTileMapNode?
    private var changedPositions = [Game.Position]()

    private func makeCell(column: Int, row: Int) -> SKShapeNode {
        let rect = SKShapeNode(rectOf: tileSize)
        rect.alpha = 0.5
        rect.fillColor = .black
        rect.strokeColor = .black
        rect.position = CGPoint(x: tileSize.width * CGFloat(column) + tileSize.width / 2, y: tileSize.height * CGFloat(row) + tileSize.height / 2)
        return rect
    }

    private func makeCamera() -> SKCameraNode {
        let camera = SKCameraNode()
        addChild(camera)
        return camera
    }

    private func makeConstraints() -> [SKConstraint]? {
        guard let background = background, let statusbar = statusbar else { return nil }
        let xRange = SKRange(lowerLimit: size.midPoint.x + background.position.x,
                             upperLimit: background.size.width + background.position.x - size.midPoint.x)
        let yRange = SKRange(lowerLimit: size.midPoint.y + background.position.y - statusbar.size.height,
                            upperLimit: background.position.y + background.size.height - size.midPoint.y)
        let constraint = SKConstraint.positionX(xRange, y: yRange)
        return [constraint]
    }

    private func makeBoard(size boardSize: Game.BoardSize) -> SKTileMapNode {
        let texture = SKTexture(image: #imageLiteral(resourceName: "grass"))
        let tileDefinition = SKTileDefinition(texture: texture, size: tileSize)
        let tilegroup = SKTileGroup(tileDefinition: tileDefinition)
        let tileset = SKTileSet(tileGroups: [tilegroup])
        let background = SKTileMapNode(tileSet: tileset, columns: boardSize.columns, rows: boardSize.rows, tileSize: tileSize, fillWith: tilegroup)
        for column in 0..<boardSize.columns {
            for row in 0..<boardSize.rows {
                let cell = makeCell(column: column, row: row)
                background.addChild(cell)
                map.append(cell)
            }
        }
        background.anchorPoint = .zero
        addChild(background)
        return background
    }

    private func registerGestureRecognizers(view: UIView) {
        let longPressGestureRecognizer = UILongPressGestureRecognizer()
        longPressGestureRecognizer.addTarget(self, action: #selector(MineSweeperBoard.didLongPress(_:)))
        view.addGestureRecognizer(longPressGestureRecognizer)

        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(MineSweeperBoard.didTap(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer()
        panGestureRecognizer.addTarget(self, action: #selector(MineSweeperBoard.didPan(_:)))
        view.addGestureRecognizer(panGestureRecognizer)

        let pinchGestureRecognizer = UIPinchGestureRecognizer()
        pinchGestureRecognizer.addTarget(self, action: #selector(MineSweeperBoard.didPinch(_:)))
        view.addGestureRecognizer(pinchGestureRecognizer)
    }

    override func didMove(to view: SKView) {
        if let game = game {
            background = makeBoard(size: game.boardSize)
            background?.position = CGPoint(x: 0, y: 0)
        }
        statusbar = Statusbar(size: CGSize(width: size.width, height: 60))
        statusbar?.position = CGPoint(x: 0 - size.midPoint.x, y: 0 - size.midPoint.y)
        camera = makeCamera()
        camera?.position = size.midPoint.moved(by: CGVector(dx: 0, dy: -60))
        camera?.addChild(statusbar!)
        camera?.constraints = makeConstraints()
        registerGestureRecognizers(view: view)
    }

    override func didChangeSize(_ oldSize: CGSize) {
        camera?.constraints = makeConstraints()
        statusbar?.position = CGPoint(x: 0 - size.midPoint.x, y: 0 - size.midPoint.y)
        statusbar?.size = CGSize(width: size.width, height: 60)
    }

    override func update(_ currentTime: TimeInterval) {
        guard let game = game else { return }
        statusbar?.timeElapsedLabel = String(format: "%0i:%02i", game.secondsElapsed.minutes, game.secondsElapsed.seconds)
        statusbar?.minesLabel = String(format: "%03i", game.flagged)

        if changedPositions.count > 0 {
            changedPositions.forEach { drawTile(position: $0) }
            changedPositions.removeAll()
            switch game.state {
            case .playing:
                let action = SKAction.playSoundFileNamed("switch", waitForCompletion: false)
                run(action)
                statusbar?.statusLabel = "😨"
            case .lost:
                let action = SKAction.playSoundFileNamed("explosion", waitForCompletion: false)
                run(action)
                camera?.addChild(makeGameOverLabel(with: "You lost!"))
                revealBoard()
                statusbar?.statusLabel = "🤯"
            case .won:
                camera?.addChild(makeGameOverLabel(with: "You won!"))
                revealBoard()
                statusbar?.statusLabel = "😄"
            }
        }
    }

    private func makeGameOverLabel(with text: String) -> SKLabelNode {
        let gameOver = SKLabelNode(text: text)
        gameOver.fontColor = .red
        gameOver.fontSize = 40
        gameOver.fontName = "ChalkboardSE-Bold"
        gameOver.name = "Game Over"
        return gameOver
    }

    private func revealBoard() {
        guard let game = game else { return }
        for row in 0..<game.boardSize.rows {
            for column in 0..<game.boardSize.columns {
                let position = Game.Position(column: column, row: row)
                let field = game[field: position]
                if (field.explorationState == .flagged && field.fieldState != .mine)
                    || (field.explorationState != .flagged && field.fieldState == .mine){
                    drawTile(position: position)
                }
            }
        }
    }

    override func willMove(from view: SKView) {
        view.gestureRecognizers?.forEach {
            view.removeGestureRecognizer($0)
        }
        removeAllActions()
        removeAllChildren()
    }

    private func getTile(from gesture: UIGestureRecognizer) -> Game.Position? {
        guard let statusbar = statusbar, let background = background else {
            return nil
        }
        let position = convertPoint(fromView: gesture.location(in: self.view))
        let nodes = self.nodes(at: position)
        guard !nodes.contains(statusbar) && nodes.contains(background) else {
            return nil
        }
        let column = background.tileColumnIndex(fromPosition: position)
        let row = background.tileRowIndex(fromPosition: position)
        return Game.Position(column: column, row: row)
    }

    private func isInBounds(position: Game.Position) -> Bool {
        guard let boardSize = game?.boardSize else {
            return false
        }
        return position.row >= 0 && position.row < boardSize.rows
            && position.column >= 0 && position.column < boardSize.columns
    }

    private func color(for field: Game.Field, in state: Game.GameState) -> UIColor {
        if state == .playing {
            switch field.explorationState {
            case .visited, .flagged, .marked:
                return .clear
            case .unvisited:
                return .black
            }
        } else {
            if field.isExploded || (field.explorationState == .flagged && field.fieldState == .clear) {
                return .red
            } else {
                return .clear
            }
        }
    }

    private func drawTile(position: Game.Position) {
        guard let game = game else { return }
        let field = game[field: position]
        let rect = map[position.column * game.boardSize.rows + position.row]
        rect.removeAllChildren()
        if field.explorationState != .unvisited || game.state != .playing {
            rect.fillColor = color(for: field, in: game.state)
            let label = SKLabelNode(text: getLabel(field: field, state: game.state))
            label.position = .zero
            label.fontName = UIFont.boldSystemFont(ofSize: fontSize).fontName
            label.verticalAlignmentMode = .center
            label.horizontalAlignmentMode = .center
            label.fontSize = fontSize
            label.fontColor = getColor(for: field.neighbouringMines)
            label.alpha = 1.0
            rect.addChild(label)
        } else {
            rect.fillColor = .black
        }
    }

    private func getColor(for numberOfMines: Int) -> UIColor {
        guard numberOfMines >= 0 && numberOfMines <= colors.count else {
            return .black
        }
        return colors[numberOfMines]
    }

    @objc func didTap(_ sender: UITapGestureRecognizer) {
        guard let game = game, let position = getTile(from: sender),
            isInBounds(position: position) else { return }
        changedPositions.append(contentsOf: game.toggleMark(position: position))
    }

    @objc func didLongPress(_ sender: UILongPressGestureRecognizer) {
        guard let game = game, let position = getTile(from: sender),
            sender.state == .began && isInBounds(position: position) else { return }
        changedPositions.append(contentsOf: game.play(position: position))
    }

    @objc func didPan(_ sender: UIPanGestureRecognizer) {
        let vector = sender.translation(in: view).applying(CGAffineTransform(scaleX: 1 / zoomFactor, y: 1 / zoomFactor))
        guard let camera = camera else { return }
        let position = camera.position
        camera.position = CGPoint(x: position.x - vector.x, y: position.y + vector.y)
        sender.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
    }

    @objc func didPinch(_ sender: UIPinchGestureRecognizer) {
        var scale = sender.scale * zoomFactor
        if scale < 0.5 { scale = 0.5 }
        if scale > 1.0 { scale = 1.0 }
        switch sender.state {
        case .began, .changed:
            camera?.setScale(1 / scale)
        case .ended:
            camera?.setScale(1 / scale)
            zoomFactor = scale
        default:
            break
        }
    }

    private func getLabel(field: Game.Field, state: Game.GameState) -> String {
        switch state {
        case .playing:
            switch field.explorationState {
            case .flagged: return "🚩"
            case .marked: return "❓"
            case .visited:
                switch field.fieldState {
                case .mine: return "💥"
                case .clear: return field.neighbouringMines > 0 ? "\(field.neighbouringMines)" : " "
                }
            case .unvisited: return " "
            }
        case .lost, .won:
            switch field.fieldState {
            case .mine:
                switch field.explorationState {
                case .visited: return "💥"
                case .flagged: return "🚩"
                case .unvisited, .marked: return "💣"
                }
            case .clear:
                return field.neighbouringMines > 0 ? "\(field.neighbouringMines)" : " "
            }
        }
    }
}

private extension TimeInterval {
    var seconds: Int {
        return Int(self.truncatingRemainder(dividingBy: 60))
    }
    var minutes: Int {
        return Int(self / 60)
    }
}

private extension SKTileMapNode {
    var size: CGSize {
        return CGSize(width: tileSize.width * CGFloat(numberOfColumns), height: tileSize.height * CGFloat(numberOfRows))
    }
}

private extension CGSize {
    var midPoint: CGPoint {
        return CGPoint(x: width / 2, y: height / 2)
    }
}

private extension CGPoint {
    func moved(by: CGVector) -> CGPoint {
        return CGPoint(x: x + by.dx, y: y + by.dy)
    }
}
