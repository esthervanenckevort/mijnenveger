//
//  ViewController.swift
//  Mijnenveger
//
//  Created by David van Enckevort on 11-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit
import SpriteKit

struct GameSettings {
    let boardSize: Game.BoardSize
    let difficulty: Game.Difficulty

    var userInfo: [AnyHashable: Any] {
        return [ keys.boardSize: boardSize,
                 keys.difficulty: difficulty ]
    }

    private struct keys {
        static let difficulty = "difficulty"
        static let boardSize = "boardSize"
    }

    static func from(userInfo: [AnyHashable : Any]) -> GameSettings? {
        guard let difficulty = userInfo[keys.difficulty] as? Game.Difficulty,
            let boardSize = userInfo[keys.boardSize] as? Game.BoardSize
            else { return nil }
        return GameSettings(boardSize: boardSize, difficulty: difficulty)
    }
}

class ViewController: UIViewController {
    private var board: MineSweeperBoard?
    private var game: Game?

    @IBOutlet weak var boardView: SKView!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: Notification.newGame, object: nil, queue: .main, using: newGame(_:))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if board == nil {
            newGame(boardSize: .small, difficulty: .easy)
        }
    }

    @IBAction func newGame(segue: UIStoryboardSegue) {

    }
    @IBAction func cancelNewGame(segue: UIStoryboardSegue) {

    }

    private func newGame(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let msg = GameSettings.from(userInfo: userInfo)
            else { return }
        newGame(boardSize: msg.boardSize, difficulty: msg.difficulty)
    }

    private func newGame(boardSize: Game.BoardSize, difficulty: Game.Difficulty) {
        board = MineSweeperBoard(size: boardView.frame.size)
        board?.scaleMode = .resizeFill
        game = Game(boardSize: boardSize, difficulty: difficulty)
        board?.game = game
        boardView.presentScene(board)
    }

}

