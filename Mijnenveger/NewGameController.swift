//
//  NewGameController.swift
//  Mijnenveger
//
//  Created by David van Enckevort on 12-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import UIKit

class NewGameController: UIViewController {
    var boardSize: Game.BoardSize = Game.BoardSize.small
    var difficulty: Game.Difficulty = Game.Difficulty.easy

    override func viewWillLayoutSubviews() {
        let width = view.subviews.reduce(0) { return $0 > $1.frame.maxX ? $0 : $1.frame.maxX }
        let height = view.subviews.reduce(CGFloat(0)) {
            if $1.intrinsicContentSize.height == -1 {
                if let view = $1.subviews.first as? UITableView {
                    return $0 + view.contentSize.height
                }
            }
            return $0 + $1.intrinsicContentSize.height
        }
        preferredContentSize = CGSize(width: width, height: height)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {
            fatalError("Segue identifier not set.")
        }
        switch identifier {
        case "embedBoardSize":
            if let vc = segue.destination as? ConfigurationTableViewController {
                vc.observer = { [weak self] in self?.boardSize = $0 as! Game.BoardSize }
                vc.isInitialValue = { [weak self] in self?.boardSize.name == $0.name }
                vc.options = Game.BoardSize.values
                vc.title = Game.BoardSize.title
            }
        case "embedDifficulty":
            if let vc = segue.destination as? ConfigurationTableViewController {
                vc.observer = {  [weak self] in self?.difficulty = $0 as! Game.Difficulty }
                vc.isInitialValue = {  [weak self] in self?.difficulty.name == $0.name }
                vc.options = Game.Difficulty.values
                vc.title = Game.Difficulty.title
            }
        case "newGame":
            let userInfo = GameSettings(boardSize: boardSize, difficulty: difficulty).userInfo
            NotificationCenter.default.post(name: Notification.newGame, object: self, userInfo: userInfo)
        default:
            break
        }
    }
}

class ConfigurationTableViewController: UITableViewController {
    var options: [Configuration] = []
    var observer: ((Configuration) -> ())?
    var isInitialValue: ((Configuration) -> (Bool))?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let isInitialValue = isInitialValue else { return }
        for row in 0..<options.count {
            if isInitialValue(options[row]) {
                tableView.selectRow(at: IndexPath(row: row, section: 0), animated: true, scrollPosition: .top)
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.title
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameModeCell", for: indexPath)
        cell.textLabel?.text = options[indexPath.row].name
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        observer?(options[indexPath.row])
    }
}

class GameModeCell: UITableViewCell {
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
    }
}
