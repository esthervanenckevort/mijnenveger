//
//  PlayingField.swift
//  Mijnenveger
//
//  Created by David van Enckevort on 11-11-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

protocol Configuration {
    static var title: String { get }
    static var values: [Configuration] { get }
    var name: String { get }
}

class Game {
    struct Position {
        let column: Int
        let row: Int
        func previousRow() -> Position {
            return Position(column: column, row: row - 1)
        }
        func nextRow() -> Position {
            return Position(column: column, row: row + 1)
        }
        func previousColumn() -> Position {
            return Position(column: column - 1, row: row)
        }
        func nextColumn() -> Position {
            return Position(column: column + 1, row: row)
        }
    }

    struct Field {
        var fieldState: FieldState
        var explorationState: ExplorationState
        var neighbouringMines: Int
        var isExploded: Bool {
            return fieldState == .mine && explorationState == .visited
        }
        var isDetected: Bool {
            return fieldState == .mine && explorationState == .flagged
        }
        var isCleared: Bool {
            return fieldState == .clear && explorationState == .visited
        }
        var isChecked: Bool {
            return isDetected || isCleared
        }
    }

    enum FieldState {
        case mine
        case clear
    }

    enum ExplorationState {
        case visited
        case unvisited
        case flagged
        case marked
    }

    enum GameState {
        case playing
        case lost
        case won
    }

    struct Difficulty: Configuration {
        static let easy = Difficulty(name: "Easy", percentageOfMines: 10)
        static let medium = Difficulty(name: "Medium", percentageOfMines: 15)
        static let hard = Difficulty(name: "Hard", percentageOfMines: 20)
        static let values: [Configuration] = [ easy, medium, hard ]
        static let title: String = "Difficulty"

        let name: String
        let percentageOfMines: Int
    }

    struct BoardSize: Configuration {
        static let title: String = "Board Size"
        static let small = BoardSize(rows: 10, columns: 10, name: "Small")
        static let medium = BoardSize(rows: 20, columns: 20, name: "Medium")
        static let large = BoardSize(rows: 30, columns: 30, name: "Large")
        static let values: [Configuration] = [ small, medium, large ]

        let rows: Int
        let columns: Int
        let name: String
        var numberOffields: Int {
            return rows * columns
        }
    }

    let boardSize: BoardSize
    private (set) var state: GameState
    private let mines: Int
    private let difficulty: Difficulty
    private (set) var secondsElapsed: TimeInterval
    private (set) var flagged: Int
    private var detected: Int
    private var unchecked: Int
    private var fields: [Field]
    private (set) subscript(field at: Position) -> Field {
        get {
            return fields[at.row * boardSize.columns + at.column]
        }
        set {
            let oldValue = fields[at.row * boardSize.columns + at.column]
            fields[at.row * boardSize.columns + at.column] = newValue
            calculateStateChange(oldValue: oldValue, newValue: newValue)
       }
    }

    private func calculateStateChange(oldValue: Field, newValue: Field) {
        if oldValue.explorationState == .flagged && newValue.explorationState != .flagged {
            flagged -= 1
            if oldValue.isDetected {
                detected -= 1
            }
        }
        if newValue.explorationState == .flagged && oldValue.explorationState != .flagged {
            flagged += 1
            if newValue.isDetected {
                detected += 1
            }
        }
        if oldValue.isChecked && !newValue.isChecked {
            unchecked += 1
        }
        if !oldValue.isChecked && newValue.isChecked {
            unchecked -= 1
        }
        if newValue.isExploded {
            state = .lost
        } else if detected == mines && unchecked == 0 {
            state = .won
        }
    }

    private func isInBounds(_ position: Position) -> Bool {
        return position.row >= 0 && position.row < boardSize.rows
            && position.column >= 0 && position.column < boardSize.columns
    }

    private func getNeighbours(position: Position) -> [Position] {
        let neighbours = [ position.previousRow().previousColumn(), position.previousRow(), position.previousRow().nextColumn(),
                           position.previousColumn(), position.nextColumn(),
                           position.nextRow().previousColumn(), position.nextRow(), position.nextRow().nextColumn()]
        return neighbours.filter(isInBounds(_:))
    }

    private func visitNeighbours(position: Position) -> [Position] {
        let neighbours = getNeighbours(position: position)
        let flagged = neighbours.filter { self[field: $0].explorationState == .flagged}.count

        guard self[field: position].explorationState == .visited,
            self[field: position].fieldState == .clear,
            self[field: position].neighbouringMines == flagged
            else { return [] }

        return neighbours
            .filter { self[field: $0].explorationState == .unvisited }
            .flatMap { self.play(position: $0) }
    }

    func toggleMark(position: Position) -> [Position] {
        guard state == .playing else { return []}
        switch self[field: position].explorationState {
        case .visited:
            return visitNeighbours(position: position)
        case .unvisited:
            self[field: position].explorationState = .flagged
        case .flagged:
            self[field: position].explorationState = .marked
        case .marked:
            self[field: position].explorationState = .unvisited
        }
        return [position]
    }

    func play(position: Position) -> [Position] {
        guard state == .playing, self[field: position].explorationState == .unvisited else { return [] }
        var changed = [position]
        self[field: position].explorationState = .visited
        if self[field: position].fieldState == .clear
            && self[field: position].neighbouringMines == 0 {
            changed.append(contentsOf: visitNeighbours(position: position))
        }
        return changed
    }

    init(boardSize: BoardSize, difficulty: Difficulty) {
        self.boardSize = boardSize
        self.difficulty = difficulty
        self.detected = 0
        self.flagged = 0
        self.unchecked = boardSize.numberOffields
        self.state = .playing
        self.mines = difficulty.percentageOfMines * boardSize.numberOffields / 100
        self.secondsElapsed = 0
        let defaultField = Field(fieldState: .clear, explorationState: .unvisited, neighbouringMines: 0)
        fields = Array<Field>(repeating: defaultField, count: boardSize.numberOffields)
        var potentialMines = Array<Int>(0..<boardSize.numberOffields)
        for _ in 0..<mines {
            fields[potentialMines.remove(at: Int.random(potentialMines.count))].fieldState = .mine
        }
        for row in 0..<boardSize.rows {
            for column in 0..<boardSize.columns {
                let position = Position(column: row, row: column)
                if self[field: position].fieldState == .mine {
                    getNeighbours(position: position).forEach { self[field: $0].neighbouringMines += 1 }
                }
            }
        }
        NotificationCenter.default.addObserver(forName: Notification.time, object: nil, queue: .main) {
            [weak self] (notification) in
            guard let game = self, game.state == .playing else { return }
            game.secondsElapsed += 1
        }
    }
}

private extension Int {
    static func random(_ max: Int) -> Int {
        return Int(arc4random_uniform(UInt32(max)))
    }
}
